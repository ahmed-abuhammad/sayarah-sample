<?php

/* @var $this yii\web\View */

$this->title = 'My Google Drive Files';
?>
<div class="site-google-drive">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <!--
                    A simple table to view the information
                    of the google drive files
                -->
                <table class="table table-striped">
                    <tr>
                        <th>Title</th>
                        <th>Thmbnail Link</th>
                        <th>Embed Link</th>
                        <th>Modified Date</th>
                        <th>File Size</th>
                        <th>Owner Names</th>
                    </tr>
                    <?php foreach ($files as $file): ?>
                        <tr>
                            <td><?= $file->name; ?></td>
                            <td><img width="100" src="<?= $file->thumbnailLink; ?>"></td>
                            <td><a target="_blank" href="<?= $file->webContentLink; ?>">URL</a></td>
                            <td><?= explode("T", $file->modifiedTime)[0]; ?></td>
                            <td><?= floor($file->size / 1000000); ?>MB</td>
                            <td><?= implode(", ", array_map(function($owner) { return $owner["displayName"]; }, $file->owners)); ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>

    </div>
</div>
