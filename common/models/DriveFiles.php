<?php
namespace common\models;

use Yii;
use yii\base\Model;

use Google_Client;
use Google_Service_Drive;

// This model represents our google drive files data
class DriveFiles extends Model
{
	private $_client;

	// We set the token in the google api client
	public function setAccessToken($accessToken)
	{
		if ($this->_client)
		{
			$this->_client->setAccessToken($accessToken);
		}
		else
		{
			$this->getGoogleClient();
			$this->_client->setAccessToken($accessToken);
		}
	}

	// This method initializes the google api client
	public function getGoogleClient()
	{
		if ($this->_client)
		{
			return $this->_client;
		}

		// Create a new google api client instance
		$client = new Google_Client();

		// Set the oauth2 secret file
		$client->setAuthConfig(Yii::getAlias("@common/assets/google/client_secret.json"));

		// We only want to read the files and their meta data
		$client->setScopes([Google_Service_Drive::DRIVE_READONLY, Google_Service_Drive::DRIVE_METADATA_READONLY]);

		// Specify the redirection url after authenticating with google
		// Note: The "google.sayarah.com" was the host I used locally
		$client->setRedirectUri("http://google.sayarah.com/oauth2-callback");
		$client->setAccessType("offline");

		$this->_client = $client;
		return $this->_client;
	}

	// This public method is used to get the data
	public function getData()
	{
		$client = $this->getGoogleClient();

		// Initialize the google drive service and attach it to the
		// google api client
		$drive = new Google_Service_Drive($client);
		return $this->retrieveAllFiles($drive);
	}

	// A helper method to get all the files for
	// The specified google drive service
	private function retrieveAllFiles($service)
	{
		$result = array();
		$pageToken = NULL;

		do
		{
			try
			{
				$parameters = array('fields' => '*');
				if ($pageToken)
				{
					$parameters['pageToken'] = $pageToken;
				}
				$files = $service->files->listFiles($parameters);

				$result = array_merge($result, $files->getFiles());
				$pageToken = $files->getNextPageToken();
			}
			catch (Exception $e)
			{
				print "An error occurred: " . $e->getMessage();
				$pageToken = NULL;
			}
		} while ($pageToken);

		return $result;
	}
}